require 'bundler/capistrano'

#$:.unshift("#{ENV["HOME"]}/.rvm/lib")
$:.unshift(File.expand_path('./lib', ENV['rvm_path']))
require "rvm/capistrano"

set :rvm_type, :user
#set :rvm_type, :system
set :application, "resizeimage.org"
set :repository,  "git@github.com:agaskell/resizeimage.git"

set :scm, :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

set :user, "andy"
set :port, 30000
set :use_sudo, false
set :deploy_to, "/home/andy/sites/resizeimage.org"


role :web, "resizeimage.org"                          # Your HTTP server, Apache/etc
role :app, "resizeimage.org"                          # This may be the same as your `Web` server
role :db,  "resizeimage.org", :primary => true # This is where Rails migrations will run

# If you are using Passenger mod_rails uncomment this:
# if you're still using the script/reapear helper you will need
# these http://github.com/rails/irs_process_scripts

 namespace :deploy do
   task :start do ; end
   task :stop do ; end
   task :restart, :roles => :app, :except => { :no_release => true } do
     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
   end
 end

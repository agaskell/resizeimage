require 'test_helper'

class ResizableImagesControllerTest < ActionController::TestCase
  setup do
    @resizable_image = resizable_images(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:resizable_images)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create resizable_image" do
    assert_difference('ResizableImage.count') do
      post :create, :resizable_image => @resizable_image.attributes
    end

    assert_redirected_to resizable_image_path(assigns(:resizable_image))
  end

  test "should show resizable_image" do
    get :show, :id => @resizable_image.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @resizable_image.to_param
    assert_response :success
  end

  test "should update resizable_image" do
    put :update, :id => @resizable_image.to_param, :resizable_image => @resizable_image.attributes
    assert_redirected_to resizable_image_path(assigns(:resizable_image))
  end

  test "should destroy resizable_image" do
    assert_difference('ResizableImage.count', -1) do
      delete :destroy, :id => @resizable_image.to_param
    end

    assert_redirected_to resizable_images_path
  end
end

class ResizableImage < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper
  attr_accessor :message, :from_email, :recipients, :x1, :y1, :x2, :y2
  attr_accessible :message, :from_email, :recipients, :image, :x1, :y1, :x2, :y2
  scope :since, lambda {|time| {:conditions => ["created_at > ?", time] }}
  after_update :reprocess_image, :if => :cropping_or_resizing?


  has_attached_file :image, :styles => {:original => "yeah!" },  :processors => [:resizer] 

  #has_attachment :content_type => :image, 
  #               :storage => :file_system, 
  #               :path_prefix => 'public/system',
  #               :max_size => 5.megabytes,
  #               :processor => :MiniMagick
 
  #validates_as_attachment

  def self.destroy_old_images
    ResizableImage.since(1.week.ago).each { |r| r.destroy }
  end
  
  def human_size
    number_to_human_size(image_file_size)
  end
  
  def to_simple_json
    self.to_json(:only => [:height, :width], :methods => [:url, :human_size])  
  end

  def url
    image.url
  end

  def cropping?
    !x1.blank? && !y1.blank? && !x2.blank? && !y2.blank?
  end

  def resizing?
    !width.blank? && !height.blank?
  end

  def cropping_or_resizing?
    cropping? || resizing? 
  end

  private
  def reprocess_image
    puts "cropping? = #{cropping?}"
    puts "resizing? = #{resizing?}"
  
    image.reprocess!

  end

end


module Paperclip
  class Resizer < Thumbnail
    def transformation_command 
      target = @attachment.instance

      if target.cropping?
        puts "croppin"
        "  -crop #{target.w}x#{target.h}+#{target.x}+#{target.y}"
      else
        puts "not croppin"
      end

    end
  end

end

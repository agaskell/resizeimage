module ResizableImagesHelper
  def resizable_image_tag(public_path, alt)
    t = Time.now.to_i
    "<img src='#{public_path}?#{t}' id='user' class='resizable' alt=#{alt} />" + hidden_tag(public_path, t)
  end
  
  def basic_image_tag(public_path, alt)
    helper_image_tag(public_path, alt, 'user')
  end

  def crop_image_tag(public_path, alt)
    helper_image_tag(public_path, alt, 'crop')
  end
  
  private
  
  def helper_image_tag(public_path, alt, id)
    t = Time.now.to_i
    "<img src='#{public_path}?#{t}' id='#{id}' alt='#{alt}' />" + hidden_tag(public_path, t)
  end
  
  def hidden_tag(public_path, time)
    "<img src='#{public_path}?#{time}' id='hidden' alt='avert your eyes!' />"
  end
end

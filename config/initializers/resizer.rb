module Paperclip
  class Resizer < Thumbnail
    def transformation_command 
      target = @attachment.instance

      if target.cropping?
        w = (target.x2.to_i - target.x1.to_i).abs
        h = (target.y2.to_i - target.y1.to_i).abs
        "  -crop #{w}x#{h}+#{target.x1}+#{target.y1}"
      elsif target.resizing?
        " -resize #{target.width}x#{target.height}!"
      end

    end
  end

end

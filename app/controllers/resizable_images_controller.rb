class ResizableImagesController < ApplicationController
  before_filter :load_image, :except => [ :create, :new ]
  before_filter :update_image, :only => [ :update, :advanced_update ]
  protect_from_forgery :except => [ :create ]  
  caches_page :new   

  def create
    @image = ResizableImage.new(params[:image])
    if @image.save
      flash[:notice] = 'Image was successfully created.'
      redirect_to @image      
    else
      render :action => :new
    end
  end

  def new
    @image = ResizableImage.new
    render(:layout => "application")
  end

  def show
    respond_to do |format|
      format.html 
      format.js   { render :json => @image.to_simple_json }
    end  
  end

  def update
    respond_to do |format|
      format.html { redirect_to resizable_image_path(@image) }
      format.js   { render :json => @image.to_simple_json }
    end  
  end

  def advanced_update
    respond_to do |format|
      format.html { redirect_to advanced_edit_resizable_image_path(@image) }
      format.js   { render :json => @image.to_simple_json }
    end 
  end

  def download
    send_file @image.image.path 
  end

  def crop_update
    #@image.with_image { |raw_image| @image.crop_image(raw_image, params[:x1].to_i, params[:y1].to_i, params[:x2].to_i, params[:y2].to_i) }
    @image.update_attributes(params[:resizable_image]) 
    #@image.save

    respond_to do |format|
      format.html { redirect_to crop_resizable_image_path(@image) }
      format.js   { render 'crop', :layout => false }
    end 
  end

  def send_email
    #message = EmailMessage.new(params[:email_message])
    p = ResizableImage.new(params[:image])
    mail = ImageMailer.mail_image(@image, p.message, p.from_email, p.recipients).deliver
    flash[:notice] = 'Email was successfully sent.'

    respond_to do |format|
      format.html { redirect_to @image }
      format.js   { render :json => @image.to_simple_json }
    end 
    
  rescue
    flash[:error] = 'Email was unable to send.'
    redirect_to email_resizable_image_path(@image)
  end

private 

  def load_image
    @image = params[:filename] ? ResizableImage.find_by_filename(params[:filename]) : ResizableImage.find(params[:id])
  end

  def update_image
    @image.width = params[:x]
    @image.height = params[:y]
    @image.save
    #@image.with_image { |raw_image| @image.resize_image(raw_image, [params[:x].to_i, params[:y].to_i]) }
    #@image.save
  end

end
